﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RPG
{
    public delegate void SimpleHandle();
    public delegate void SimpleHandle<T>(T arg);

    public static class MonoBehaviourExtensions
    {
public static T FindComponent<T>(this MonoBehaviour sourse) where T: Component
        {
            var component = sourse.GetComponent<T>();
#if UNITY_EDITOR
            if(component == null)
            {
                Editor.EditorExtensions.LogError($"Component : <b> {typeof(T).Name}</b> not found on GameObject : <i>{sourse.name}</i>", Editor.PriorityMessageType.Critical);
            }
#endif
            return component;
        }
    }
}