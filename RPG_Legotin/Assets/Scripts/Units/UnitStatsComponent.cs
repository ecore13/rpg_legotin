﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RPG.Units
{
    public class UnitStatsComponent : MonoBehaviour
    {

        [Range(0f, 10f)]
        public float MoveSpeed = 3f;
        public float MaxHealth = 10f;
        public SideType SideType;
        public string Name;


        public float _calldownShieldAttack;
        
        public float _currentCalldown;

        public float CurrentHealth { get;  set; }
        private void Update()
        {
            if (_currentCalldown > 0f)
            {
                _currentCalldown -= Time.deltaTime;
            }
        }
        private void Start()
        {
            CurrentHealth = MaxHealth;
        }
    }
}