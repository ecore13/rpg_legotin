﻿#if UNITY_EDITOR
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace RPG.Editor
{
    public static class EditorExtensions
    {
        private static Dictionary<PriorityMessageType, string> _dic;
        public static void LogError(object message, PriorityMessageType type = PriorityMessageType.None)
        {
            switch (type)
            {
                case PriorityMessageType.None:
                    Debug.LogError(message);
                    break;
                case PriorityMessageType.Low:
                    Debug.LogError(string.Concat(_dic[type],message));
                    break;
                case PriorityMessageType.Critical:
                    Debug.LogError(string.Concat(_dic[type], message));
                    EditorApplication.isPaused = true;
                    break;
            }

        }
        static EditorExtensions()
        {
            _dic = new Dictionary<PriorityMessageType, string>()
            {
                {PriorityMessageType.None, string.Empty },
                {PriorityMessageType.Low,"<color=#005500>[Low]</color>" },
                {PriorityMessageType.Critical, "<color=#880000>[Critical]</color>" }
                
                
            };
        }

    }
    public static class EditorConstants
    {
        public static readonly string FocusTargetPointName = "Neck";
    }
    public enum PriorityMessageType : byte
    {
        None,
        Low,
        Critical,
    }
}
#endif