﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestTimeController : MonoBehaviour
{
   public void TimeChange(float value)
    {
        Time.timeScale = value;
    }
}
