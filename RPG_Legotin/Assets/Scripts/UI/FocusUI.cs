﻿using RPG.Units;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace RPG.UI
{
    public class FocusUI : MonoBehaviour
    {

        private Image _image;

        [SerializeField]
        private Color _defaultColor;
        [SerializeField]
        private Color _friendlyColor;
        [SerializeField]
        private Color _enemyColor;
        [Space, SerializeField]
        private TextMeshProUGUI _nameText;
        [SerializeField]
        private TextMeshProUGUI _healthText;
        [Space, SerializeField, Range(10f, 500f)]
        private float _maxDistance = 500f;

        private void Start()
        {
            _image = this.FindComponent<Image>();
        }
        void LateUpdate()
        {
            
            var ray = Camera.main.ScreenPointToRay(transform.position);
            UnitStatsComponent stats;

            if (Physics.Raycast(ray, out var hit, _maxDistance))
            {
                stats = hit.transform.GetComponent<UnitStatsComponent>();// если попали в какого то лучем
                if (stats == null)
                {
                    ClearFocus();
                    return;
                }
            }
            else
            {
                ClearFocus();
                return;// если не дострелили на растояние 500 м
            }

            _nameText.text = stats.Name;
            _healthText.text = string.Concat(stats.CurrentHealth, '/', stats.MaxHealth);
            if (stats.SideType == SideType.Friendly)
            {
                _image.color = _friendlyColor;
                _nameText.color = _friendlyColor;
            }
            if (stats.SideType == SideType.Enemy)
            {

                 _image.color = _enemyColor;
                 _nameText.color = _enemyColor;
            }

        }
        private void ClearFocus()
        {
           _image.color = _defaultColor;
           _nameText.color = _defaultColor;

            _nameText.text = string.Empty;
            _healthText.text = string.Empty;
        }
    }

}