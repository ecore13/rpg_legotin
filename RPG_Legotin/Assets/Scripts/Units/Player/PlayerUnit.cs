﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RPG.Units.Player
{
    public class PlayerUnit : Unit
    {
        private CameraComponent _camera;
        private WeaponSetComponent _sets;
        [SerializeField, Range(5f, 10f)]
        private float _rotateSpeed;

        //извлечено ли оружие
        private bool _isArm; //todo



        private void OnRearms(WeaponType type)
        {
            _sets.WeaponType = type;
            switch (type)
            {
                case WeaponType.None://todo
                    break;
                case WeaponType.SwordAndShield:
                    _animator.SetLayerWeight(_animator.GetLayerIndex("SwordAndShield"), 1f);
                    _animator.SetLayerWeight(_animator.GetLayerIndex("Bow"), 0f);
                    _animator.SetLayerWeight(_animator.GetLayerIndex("GreatSword"), 0f);
                    _animator.SetLayerWeight(_animator.GetLayerIndex("Mage"), 0f);
                    break;
                case WeaponType.Bow:
                    _animator.SetLayerWeight(_animator.GetLayerIndex("SwordAndShield"), 0f);
                    _animator.SetLayerWeight(_animator.GetLayerIndex("Bow"), 1f);
                    _animator.SetLayerWeight(_animator.GetLayerIndex("GreatSword"), 0f);
                    _animator.SetLayerWeight(_animator.GetLayerIndex("Mage"), 0f);
                    break;
                case WeaponType.GreatSword:
                    _animator.SetLayerWeight(_animator.GetLayerIndex("SwordAndShield"), 0f);
                    _animator.SetLayerWeight(_animator.GetLayerIndex("Bow"), 0f);
                    _animator.SetLayerWeight(_animator.GetLayerIndex("GreatSword"), 1f);
                    _animator.SetLayerWeight(_animator.GetLayerIndex("Mage"), 0f);
                    break;
                case WeaponType.Magic:
                    _animator.SetLayerWeight(_animator.GetLayerIndex("SwordAndShield"), 0f);
                    _animator.SetLayerWeight(_animator.GetLayerIndex("Bow"), 0f);
                    _animator.SetLayerWeight(_animator.GetLayerIndex("GreatSword"), 0f);
                    _animator.SetLayerWeight(_animator.GetLayerIndex("Mage"), 1f);
                    break;
            }
        }
        protected override void OnMove()
        {
            base.OnMove();

            ref var movement = ref _inputs.MoveDirection;

            if (Target != null)
            {
                transform.rotation = Quaternion.Euler(0f, _camera.PivotTransform.eulerAngles.y, 0f);
            }
            else if (movement.z > 0f && movement.x == 0f)
            {
                Debug.Log("forward");
                transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.Euler(0f, _camera.transform.eulerAngles.y, 0f), _rotateSpeed * Time.deltaTime);
            }
            else if (movement.z < 0f && movement.x == 0f)
            {
                transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.Euler(0f, _camera.transform.eulerAngles.y, 0f), _rotateSpeed / 2f * Time.deltaTime);
            }
        }

        protected override void Start()
        {
            base.Start();
            _camera = FindObjectOfType<CameraComponent>();
            _sets = this.FindComponent<WeaponSetComponent>();

        }
        protected override void BindingEvents(bool unbind = false)
        {
            base.BindingEvents(unbind);
            var inputs = (PlayerInputComponent)_inputs;
            if (unbind)
            {
                inputs.MeleeSetEventHandler -= () => OnRearms(WeaponType.SwordAndShield);
                inputs.RangeSetEventHandler -= () => OnRearms(WeaponType.Bow);
                return;
            }
            inputs.MeleeSetEventHandler += () => OnRearms(WeaponType.SwordAndShield);
            inputs.RangeSetEventHandler += () => OnRearms(WeaponType.Bow);

        }
    }
}