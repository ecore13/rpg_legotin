﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RPG.Units.Player
{
    public class PlayerInputComponent : UnitInputComponent
    {
        private PlayerControls _controls;

        public SimpleHandle MeleeSetEventHandler;
        public SimpleHandle RangeSetEventHandler;

        protected void CallOnMeleeSetEvent() => MeleeSetEventHandler?.Invoke();

        protected void CallOnRangeSetEvent() => RangeSetEventHandler?.Invoke();


        void Update()
        {
            var direction = _controls.Unit.Move.ReadValue<Vector2>();
            _movement = new Vector3(direction.x, 0f, direction.y);
            
        }
        private void Awake()
        {
            _controls = new PlayerControls();
            _controls.Unit.MainAction.performed += OnMainAction;
            _controls.Unit.LockTarget.performed += OnTargetLock;
            _controls.Unit.AdditionalAction.performed += OnAdditionalAction;
            _controls.Unit.MeleeSet.performed += OnMeleeSet;
            _controls.Unit.RangeSet.performed += OnRangeSet;
        }

        private void OnRangeSet(UnityEngine.InputSystem.InputAction.CallbackContext obj)
        {
            CallOnRangeSetEvent();
        }

        private void OnMeleeSet(UnityEngine.InputSystem.InputAction.CallbackContext obj)
        {
            CallOnMeleeSetEvent();
        }

        private void OnTargetLock(UnityEngine.InputSystem.InputAction.CallbackContext obj)
        {
            CallOnTargetEvent();
        }


        private void OnMainAction(UnityEngine.InputSystem.InputAction.CallbackContext obj)
        {
            CallOnMainEvent();
        }
        private void OnAdditionalAction(UnityEngine.InputSystem.InputAction.CallbackContext obj)
        {
           CallOnAdditionalEvent();
        }

        private void OnEnable()
        {
            _controls.Unit.Enable();

        }
        private void OnDisable()
        {
            _controls.Unit.Disable();
        }
        private void OnDestroy()
        {
            _controls.Unit.MainAction.performed -= OnMainAction;
            _controls.Unit.LockTarget.performed -= OnTargetLock;
            _controls.Unit.AdditionalAction.performed -= OnAdditionalAction;
            _controls.Unit.MeleeSet.performed -= OnMeleeSet;
            _controls.Unit.RangeSet.performed -= OnRangeSet;
            _controls.Dispose();
            
        }
    }

}