﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RPG.Units
{
    public class TriggerComponent : MonoBehaviour
    {
        private static Color _enableColor = new Color(1f, 0f, 0f, 0.3f);
        private static Color _disableColor = new Color(0f, 1f, 0f, 0.3f);
        private Collider _collider;
        
        private Unit _unit;
        private UnitStatsComponent _stats;

        [SerializeField]
        private int _id = 0;


        public int GetID => _id;
        public bool Enable
        {
            get => _collider.enabled;
            set => _collider.enabled = value;

        }
        public void Construct(Unit unit, UnitStatsComponent stats)
        {
            _unit = unit;
            _stats = stats;
        }
        private void OnDrawGizmos()
        {
            Gizmos.color = Enable ? _enableColor : _disableColor;
            Gizmos.DrawCube(_collider.bounds.center, _collider.bounds.size);
        }

        void Start()
        {
            if (_collider == null)
            {
                _collider = GetComponent<Collider>();
            }

            _collider.enabled = false;
            _collider.isTrigger = true;
        }
        private void OnValidate()
        {
            _collider = GetComponent<Collider>();
        }
        private void OnTriggerEnter(Collider other)
        {
            var unit = other.GetComponent<UnitStatsComponent>();
            if (unit == null) return;
            unit.CurrentHealth -= 5f;

            if (_id == 115)
            {
                
                var body = other.GetComponent<Rigidbody>();
                if (body == null) return;
                body.constraints = RigidbodyConstraints.None;

                other.transform.LookAt(_unit.transform);

                body.AddForce(-other.transform.forward * 1000f,ForceMode.Impulse);
            }

            Debug.Log(other.name);


            if (unit.CurrentHealth < 0.1f)
            {
                Destroy(unit.gameObject);
            }
        }
    }
}