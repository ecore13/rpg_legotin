﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RPG.Units
{
    
    public class UnitInputComponent : MonoBehaviour
    {
        protected Vector3 _movement;

        public ref Vector3 MoveDirection => ref _movement;//передача ссылки 

        public SimpleHandle MainEventHandler;
        public SimpleHandle AdditioanalEventHandler;
        public SimpleHandle TargetEventhandler;
        protected void CallOnMainEvent() => MainEventHandler?.Invoke();
        protected void CallOnAdditionalEvent()=> AdditioanalEventHandler?.Invoke();
        protected void CallOnTargetEvent() => TargetEventhandler?.Invoke();

        


    }
}