// GENERATED AUTOMATICALLY FROM 'Assets/Scripts/Units/Player/PlayerControls.inputactions'

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Utilities;

namespace RPG.Units.Player
{
    public class @PlayerControls : IInputActionCollection, IDisposable
    {
        public InputActionAsset asset { get; }
        public @PlayerControls()
        {
            asset = InputActionAsset.FromJson(@"{
    ""name"": ""PlayerControls"",
    ""maps"": [
        {
            ""name"": ""Unit"",
            ""id"": ""9ea3bb01-72dd-4f73-a959-1082443d595a"",
            ""actions"": [
                {
                    ""name"": ""Move"",
                    ""type"": ""Value"",
                    ""id"": ""59f93267-c751-4222-a37e-381b101bafb4"",
                    ""expectedControlType"": ""Vector2"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""MainAction"",
                    ""type"": ""Button"",
                    ""id"": ""8f949479-a8c2-4441-a1e1-0a8efe69ab89"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""AdditionalAction"",
                    ""type"": ""Button"",
                    ""id"": ""26c0360e-11f5-4b95-8577-3d6a9c22af5d"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""LockTarget"",
                    ""type"": ""Button"",
                    ""id"": ""84fb196d-bf1b-4494-b088-618f899350fe"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""MeleeSet"",
                    ""type"": ""Button"",
                    ""id"": ""e700091e-0746-4de7-a085-0d18275eebc9"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""RangeSet"",
                    ""type"": ""Button"",
                    ""id"": ""418f21e6-6f83-48ef-a8f8-31b68925c7b9"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": ""WASD"",
                    ""id"": ""b5ef9e95-476e-4c6a-821e-316daaad1895"",
                    ""path"": ""2DVector"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""up"",
                    ""id"": ""14d39d13-1e02-4872-99e5-ebc2c0b33619"",
                    ""path"": ""<Keyboard>/w"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""down"",
                    ""id"": ""daa5ad3d-691e-4fba-bb24-c8943393f371"",
                    ""path"": ""<Keyboard>/s"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""left"",
                    ""id"": ""7940e888-8f4c-4a1a-966c-9e579ca8a0a9"",
                    ""path"": ""<Keyboard>/a"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""right"",
                    ""id"": ""488caf97-de99-4eac-9d45-1da354f67db7"",
                    ""path"": ""<Keyboard>/d"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": """",
                    ""id"": ""72d0a927-e07a-4ddb-9bb5-2d4a8981e0f0"",
                    ""path"": ""<Keyboard>/e"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""MainAction"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""3b41d96e-bf57-4c74-9d58-3fe908ca51e9"",
                    ""path"": ""<Keyboard>/f"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""AdditionalAction"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""1a66259f-7b62-483a-8b11-f5f756481326"",
                    ""path"": ""<Keyboard>/q"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""LockTarget"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""19cacc7a-d644-41a2-b1f9-84bce3b38a9c"",
                    ""path"": ""<Keyboard>/1"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""MeleeSet"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""cf065d42-4e29-4c8e-a1b9-929610b0b81e"",
                    ""path"": ""<Keyboard>/2"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""RangeSet"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        },
        {
            ""name"": ""Camera"",
            ""id"": ""9fb17117-21fb-457b-900c-88070bc45b02"",
            ""actions"": [
                {
                    ""name"": ""Delta"",
                    ""type"": ""Value"",
                    ""id"": ""16ac93a3-840c-48a3-a9a9-92d49e3973fd"",
                    ""expectedControlType"": ""Vector2"",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""42e0d29d-5cf4-43db-a0b2-8c65db3e0d0e"",
                    ""path"": ""<Mouse>/delta"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Delta"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        }
    ],
    ""controlSchemes"": []
}");
            // Unit
            m_Unit = asset.FindActionMap("Unit", throwIfNotFound: true);
            m_Unit_Move = m_Unit.FindAction("Move", throwIfNotFound: true);
            m_Unit_MainAction = m_Unit.FindAction("MainAction", throwIfNotFound: true);
            m_Unit_AdditionalAction = m_Unit.FindAction("AdditionalAction", throwIfNotFound: true);
            m_Unit_LockTarget = m_Unit.FindAction("LockTarget", throwIfNotFound: true);
            m_Unit_MeleeSet = m_Unit.FindAction("MeleeSet", throwIfNotFound: true);
            m_Unit_RangeSet = m_Unit.FindAction("RangeSet", throwIfNotFound: true);
            // Camera
            m_Camera = asset.FindActionMap("Camera", throwIfNotFound: true);
            m_Camera_Delta = m_Camera.FindAction("Delta", throwIfNotFound: true);
        }

        public void Dispose()
        {
            UnityEngine.Object.Destroy(asset);
        }

        public InputBinding? bindingMask
        {
            get => asset.bindingMask;
            set => asset.bindingMask = value;
        }

        public ReadOnlyArray<InputDevice>? devices
        {
            get => asset.devices;
            set => asset.devices = value;
        }

        public ReadOnlyArray<InputControlScheme> controlSchemes => asset.controlSchemes;

        public bool Contains(InputAction action)
        {
            return asset.Contains(action);
        }

        public IEnumerator<InputAction> GetEnumerator()
        {
            return asset.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public void Enable()
        {
            asset.Enable();
        }

        public void Disable()
        {
            asset.Disable();
        }

        // Unit
        private readonly InputActionMap m_Unit;
        private IUnitActions m_UnitActionsCallbackInterface;
        private readonly InputAction m_Unit_Move;
        private readonly InputAction m_Unit_MainAction;
        private readonly InputAction m_Unit_AdditionalAction;
        private readonly InputAction m_Unit_LockTarget;
        private readonly InputAction m_Unit_MeleeSet;
        private readonly InputAction m_Unit_RangeSet;
        public struct UnitActions
        {
            private @PlayerControls m_Wrapper;
            public UnitActions(@PlayerControls wrapper) { m_Wrapper = wrapper; }
            public InputAction @Move => m_Wrapper.m_Unit_Move;
            public InputAction @MainAction => m_Wrapper.m_Unit_MainAction;
            public InputAction @AdditionalAction => m_Wrapper.m_Unit_AdditionalAction;
            public InputAction @LockTarget => m_Wrapper.m_Unit_LockTarget;
            public InputAction @MeleeSet => m_Wrapper.m_Unit_MeleeSet;
            public InputAction @RangeSet => m_Wrapper.m_Unit_RangeSet;
            public InputActionMap Get() { return m_Wrapper.m_Unit; }
            public void Enable() { Get().Enable(); }
            public void Disable() { Get().Disable(); }
            public bool enabled => Get().enabled;
            public static implicit operator InputActionMap(UnitActions set) { return set.Get(); }
            public void SetCallbacks(IUnitActions instance)
            {
                if (m_Wrapper.m_UnitActionsCallbackInterface != null)
                {
                    @Move.started -= m_Wrapper.m_UnitActionsCallbackInterface.OnMove;
                    @Move.performed -= m_Wrapper.m_UnitActionsCallbackInterface.OnMove;
                    @Move.canceled -= m_Wrapper.m_UnitActionsCallbackInterface.OnMove;
                    @MainAction.started -= m_Wrapper.m_UnitActionsCallbackInterface.OnMainAction;
                    @MainAction.performed -= m_Wrapper.m_UnitActionsCallbackInterface.OnMainAction;
                    @MainAction.canceled -= m_Wrapper.m_UnitActionsCallbackInterface.OnMainAction;
                    @AdditionalAction.started -= m_Wrapper.m_UnitActionsCallbackInterface.OnAdditionalAction;
                    @AdditionalAction.performed -= m_Wrapper.m_UnitActionsCallbackInterface.OnAdditionalAction;
                    @AdditionalAction.canceled -= m_Wrapper.m_UnitActionsCallbackInterface.OnAdditionalAction;
                    @LockTarget.started -= m_Wrapper.m_UnitActionsCallbackInterface.OnLockTarget;
                    @LockTarget.performed -= m_Wrapper.m_UnitActionsCallbackInterface.OnLockTarget;
                    @LockTarget.canceled -= m_Wrapper.m_UnitActionsCallbackInterface.OnLockTarget;
                    @MeleeSet.started -= m_Wrapper.m_UnitActionsCallbackInterface.OnMeleeSet;
                    @MeleeSet.performed -= m_Wrapper.m_UnitActionsCallbackInterface.OnMeleeSet;
                    @MeleeSet.canceled -= m_Wrapper.m_UnitActionsCallbackInterface.OnMeleeSet;
                    @RangeSet.started -= m_Wrapper.m_UnitActionsCallbackInterface.OnRangeSet;
                    @RangeSet.performed -= m_Wrapper.m_UnitActionsCallbackInterface.OnRangeSet;
                    @RangeSet.canceled -= m_Wrapper.m_UnitActionsCallbackInterface.OnRangeSet;
                }
                m_Wrapper.m_UnitActionsCallbackInterface = instance;
                if (instance != null)
                {
                    @Move.started += instance.OnMove;
                    @Move.performed += instance.OnMove;
                    @Move.canceled += instance.OnMove;
                    @MainAction.started += instance.OnMainAction;
                    @MainAction.performed += instance.OnMainAction;
                    @MainAction.canceled += instance.OnMainAction;
                    @AdditionalAction.started += instance.OnAdditionalAction;
                    @AdditionalAction.performed += instance.OnAdditionalAction;
                    @AdditionalAction.canceled += instance.OnAdditionalAction;
                    @LockTarget.started += instance.OnLockTarget;
                    @LockTarget.performed += instance.OnLockTarget;
                    @LockTarget.canceled += instance.OnLockTarget;
                    @MeleeSet.started += instance.OnMeleeSet;
                    @MeleeSet.performed += instance.OnMeleeSet;
                    @MeleeSet.canceled += instance.OnMeleeSet;
                    @RangeSet.started += instance.OnRangeSet;
                    @RangeSet.performed += instance.OnRangeSet;
                    @RangeSet.canceled += instance.OnRangeSet;
                }
            }
        }
        public UnitActions @Unit => new UnitActions(this);

        // Camera
        private readonly InputActionMap m_Camera;
        private ICameraActions m_CameraActionsCallbackInterface;
        private readonly InputAction m_Camera_Delta;
        public struct CameraActions
        {
            private @PlayerControls m_Wrapper;
            public CameraActions(@PlayerControls wrapper) { m_Wrapper = wrapper; }
            public InputAction @Delta => m_Wrapper.m_Camera_Delta;
            public InputActionMap Get() { return m_Wrapper.m_Camera; }
            public void Enable() { Get().Enable(); }
            public void Disable() { Get().Disable(); }
            public bool enabled => Get().enabled;
            public static implicit operator InputActionMap(CameraActions set) { return set.Get(); }
            public void SetCallbacks(ICameraActions instance)
            {
                if (m_Wrapper.m_CameraActionsCallbackInterface != null)
                {
                    @Delta.started -= m_Wrapper.m_CameraActionsCallbackInterface.OnDelta;
                    @Delta.performed -= m_Wrapper.m_CameraActionsCallbackInterface.OnDelta;
                    @Delta.canceled -= m_Wrapper.m_CameraActionsCallbackInterface.OnDelta;
                }
                m_Wrapper.m_CameraActionsCallbackInterface = instance;
                if (instance != null)
                {
                    @Delta.started += instance.OnDelta;
                    @Delta.performed += instance.OnDelta;
                    @Delta.canceled += instance.OnDelta;
                }
            }
        }
        public CameraActions @Camera => new CameraActions(this);
        public interface IUnitActions
        {
            void OnMove(InputAction.CallbackContext context);
            void OnMainAction(InputAction.CallbackContext context);
            void OnAdditionalAction(InputAction.CallbackContext context);
            void OnLockTarget(InputAction.CallbackContext context);
            void OnMeleeSet(InputAction.CallbackContext context);
            void OnRangeSet(InputAction.CallbackContext context);
        }
        public interface ICameraActions
        {
            void OnDelta(InputAction.CallbackContext context);
        }
    }
}
