﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace RPG.Units
{

    public abstract class Unit : MonoBehaviour
    {
        protected UnitInputComponent _inputs;
        protected Animator _animator;
        private UnitStatsComponent _stats;
        private TriggerComponent[] _colliders;
        private bool _inAnimation;
        public SimpleHandle OnTargetLostHandler;
        [SerializeField]
        private Transform _targetPoint;
        public Unit Target { get; protected set; }
        public Transform GetTargetPoint => _targetPoint;

        protected virtual void Start()
        {
            _animator = this.FindComponent<Animator>();
            _inputs = this.FindComponent<UnitInputComponent>();
            _stats = this.FindComponent<UnitStatsComponent>();
            _colliders = GetComponentsInChildren<TriggerComponent>();//todo сделать ерез findcomponent
#if UNITY_EDITOR
           // if (_colliders.Length == 0) Editor.EditorExtensions.LogError($"GameObject : <i>{name}</i> not contains {nameof(TriggerComponent)}", Editor.PriorityMessageType.Critical);
#endif
            foreach (var collider in _colliders)
            {
                collider.Construct(this, _stats);
            }

#if UNITY_EDITOR
            if (_inputs == null)// проверка подключения компонента
            {
                Editor.EditorExtensions.LogError($"unit{name} not contains {nameof(UnitInputComponent)}", Editor.PriorityMessageType.Critical);
                return;
            }
#endif
            BindingEvents();

        }

        protected virtual void Update()
        {
            OnMove();
            
        }
      
#if UNITY_EDITOR
        [ContextMenu("UpdateInternalStates")]
        private void UpdateInternalStates()
        {
            if (_targetPoint != null) return;
            _targetPoint = GetComponentsInChildren<Transform>().First(t => t.name == Editor.EditorConstants.FocusTargetPointName);
        }
#endif

        protected virtual void OnMove()
        {
            if (_inAnimation) return;
            ref var movement = ref _inputs.MoveDirection;
            _animator.SetFloat("ForwardMove", movement.z);
            _animator.SetFloat("SideMove", movement.x);

            if (movement.x == 0f && movement.z == 0f)
            {
                _animator.SetBool("Moving", false);
            }
            else
            {
                _animator.SetBool("Moving", true);
                transform.position += transform.TransformVector(movement) * _stats.MoveSpeed * Time.deltaTime;
            }
        }
      
        private void OnMainAction()//todo
        {
            if (_inAnimation) return;//если игрок в анимации атаки, то он не атакует
            _animator.SetTrigger("MainAction");
            _inAnimation = true;

        }
        private void OnAdditionalAction()
        {
            if (_inAnimation || _stats._currentCalldown > 0f) return;//если игрок в анимации атаки, то он е атакует
            _animator.SetTrigger("AdditionalAction");
            _inAnimation = true;
            _stats._currentCalldown = _stats._calldownShieldAttack;

        }
        private void OnTargetUpdate()
        {
            if (Target != null)
            {
                Target = null;
                OnTargetLostHandler?.Invoke();//todo
                return;
            }
            var units = FindObjectsOfType<UnitStatsComponent>();

            var distance = float.MaxValue;
            UnitStatsComponent target = null;
            foreach (var unit in units)
            {
                if (unit.SideType == _stats.SideType)
                {
                    continue;
                }

                var currentDistance = (unit.transform.position - transform.position).sqrMagnitude;
                if (currentDistance < distance)
                {
                    distance = currentDistance;
                    target = unit;
                }
            }

            if (target == null) Debug.LogError("Ты балбес и забыл добавить ботов");
            Target = target.GetComponent<Unit>();

        }
        protected virtual void BindingEvents(bool unbind = false)
        {
            if (unbind)
            {
                _inputs.MainEventHandler -= OnMainAction;
                _inputs.AdditioanalEventHandler -= OnAdditionalAction;
                _inputs.TargetEventhandler -= OnTargetUpdate;
                return;
            }
            _inputs.MainEventHandler += OnMainAction;
            _inputs.AdditioanalEventHandler += OnAdditionalAction;
            _inputs.TargetEventhandler += OnTargetUpdate;
        }
        private void OnDestroy()
        {

#if UNITY_EDITOR
            if (_inputs == null)// проверка подключения компонента
            {
                Editor.EditorExtensions.LogError($"unit{name} not contains {nameof(UnitInputComponent)}", Editor.PriorityMessageType.Low);
                return;
            }
#endif
            BindingEvents(true);
        }
        private void OnAnimationEnd_UnityEvent(AnimationEvent data)
        {
            _inAnimation = false;
        }
        private void OnCollider_UnityEvent(AnimationEvent data)
        {
            var collider = _colliders.FirstOrDefault(tag => tag.GetID == data.intParameter);
#if UNITY_EDITOR
            if (collider == null)
            {
                Debug.LogError($"collider with ID {data.intParameter} not found!");
                UnityEditor.EditorApplication.isPaused = true;
            }
#endif
            collider.Enable = data.floatParameter == 1f;

        }
    }
}


